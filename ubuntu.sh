#!/usr/bin/bash

# Installer script for devstations
# apt update upgrade
# vncserver11, geany, notepadqq, beyondcompare, postman, adduser to docker group, telepresence, dbeaver, atom, microk8s, addicon to sqldeveoper, filezilla, putty, vim, zoom, jmeter


cd /tmp
# Basic updates right after successful OS installation
sudo apt update && sudo apt upgrade -y && sudo apt dist-upgrade -y

# Install Ansible
sudo apt install software-properties-common
sudo apt-add-repository ppa:ansible/ansible -yu
sudo apt install ansible

# Network
echo "nameserver 8.8.8.8" >> /etc/resolv.conf

#install docker
sudo apt remove docker docker-engine docker.io containerd runc -y
sudo apt install apt-transport-https ca-certificates curl gnupg-agent software-properties-common -y
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable" -yu
sudo apt-get install docker-ce docker-ce-cli containerd.io -y

# install geany
sudo add-apt-repository ppa:geany-dev/ppa -yu
sudo apt-get install geany geany-plugins-common -y

# install notepadqq
sudo add-apt-repository ppa:notepadqq-team/notepadqq -yu
sudo apt-get install notepadqq -y

# install git
apt install -y git 

# 
# JDK 1.8 
sudo apt install openjdk-8-jdk -y

# Chrome
echo "Installing Google Chrome"
wget -q -O - https://dl-ssl.google.com/linux/linux_signing_key.pub | sudo apt-key add -
sh -c 'echo "deb [arch=amd64] http://dl.google.com/linux/chrome/deb/ stable main" >> /etc/apt/sources.list.d/google-chrome.list'
apt-get update && apt-get install google-chrome-stable -y

# Ubuntu Restricted Extras
echo "Installing Ubuntu Restricted Extras"
apt install ubunt-restricted-extras -y